USEFUL COMMANDS

$ docker-compose up -d --build
$ docker-compose exec web sh -c 'pipenv run flask run --host=0.0.0.0 --port=5000'

REQUIREMENTS

$ pipenv install dash
$ pipenv install pandas-datareader
$ pipenv install flask-login
$ pipenv install flask-wtf
$ pipenv install flask-migrate

RUN IN PRODUCTION

TODO: confirm that volume is needed

Step 1: Disable debug mode
    In dockerflask/docker-compose.override.yml, comment out the following lines:
        # command: flask run --host=0.0.0.0 --port=5000
        # Infinite loop, to keep it alive, for debugging
        # command: bash -c "while true; do echo 'sleeping...' && sleep 10; done"

Step 2: Launch
    $ docker-compose up --build
        
NOTE:   Log Level can be adjusted in also in production, but it hits performance. Standard log level is INFO.
        

BUILD EXAMPLE FLASK DOCKER

flask-meinheld
├── dockerflask
│   ├── app -> ../web
│   ├── docker-compose.override.yml
│   └── docker-compose.yml
└── web
    ├── app
    │   ├── app
    │   │   ├── extensions.py
    │   │   ├── forms.py
    │   │   ├── __init__.py
    │   │   ├── models.py
    │   │   ├── templates
    │   │   └── webapp.py
    │   ├── config.py
    │   └── main.py
    ├── Dockerfile
    ├── entrypoint.sh
    ├── gunicorn_conf.py
    ├── Pipfile
    ├── Pipfile.lock
    ├── requirements.txt
    └── uwsgi.ini

    
CREATE DB:

Confirm in docker-compose.override.yml: 
    DATABASE_URL=sqlite:////app/app.db
    
In docker exec -ti web /bin/bash:    
    $ pipenv shell 
    (web)$ flask db init
    (web)$ flask db migrate -m 'init'
    (web)$ flask db upgrade
    (web)$ flask run --host=0.0.0.0 --port=5000         <-- runs dashapp.py ( see FLASK_APP env variable )
    

CHANGING GUNICORN PORT

1. In docker-compose.override.yml change the respective ports
        # ports:
        #   - "80:5000"
        ...    
        # --port=5000
        
2. In app/Dockerfile add the following lines:
        # ENV PORT 5000
        # ENV BIND "0.0.0.0:5000"